from matplotlib import pyplot as plt
import numpy as np
from timeit import timeit
from time import clock
from Heap import Heap

my_heap = Heap()

#test heapify time.
heapify_times = list()
my_setup = """
from Heap import Heap
my_heap = Heap()

"""
for n in range(0,5000, 100):
    jumbled_numbers = list(np.arange(n))
    np.random.shuffle(jumbled_numbers)
    t = timeit(setup = my_setup+"l = %r"%(jumbled_numbers), stmt = "my_heap.heapify(l)", number=1)
    heapify_times.append(t)

plt.plot(range(0,5000, 100),heapify_times)
plt.xlabel("Input size")
plt.ylabel("Execution Time")
plt.title("Heapify function")
plt.show()

#test the time for removing from the front until empty
my_heap = Heap(jumbled_numbers)
sizes = list()
removal_times = list()

while(len(my_heap.array)>1):
    sizes.append(len(my_heap.array))
    top = my_heap.array[0]
    my_heap.remove(0)
    my_setup = "from __main__ import my_heap,top\nmy_heap.insert(top)"
    t = timeit(stmt = "my_heap.remove(0)", setup = my_setup, number=100)/100
    removal_times.append(t)
    
plt.plot(sizes, removal_times)
plt.xlabel("Heap size")
plt.ylabel("Removal time")
plt.title("Removing the root")
plt.show()

#time insertion
insertion_times = list()
for i in jumbled_numbers:
    t = timeit(stmt="my_heap.insert(i)", setup = "from __main__ import my_heap, i", number=1)
    insertion_times.append(t)
    
plt.plot(insertion_times)
plt.xlabel("Heap size")
plt.ylabel("Insertion time")
plt.show()

#check that we actually get heaps.
right_child = lambda k: 2*k+2
left_child = lambda k: 2*k+1

def is_heap(a):
    for i in range(len(a)//2-1):
        lc, rc = left_child(i), right_child(i)
        if(a[i] > a[lc]):
            return False
        if(rc < len(a) and a[i] > a[rc]):
            return False
    return True    

for n in range(0,5000, 1):
    jumbled_numbers = list(np.arange(n))
    np.random.shuffle(jumbled_numbers)
    my_heap = Heap(jumbled_numbers)
    if(is_heap(my_heap.array)):
        print(n, "passed", end="\r")
    else:
        raise ValueError("Didn't heapify properly: %r"%my_heap)
        
