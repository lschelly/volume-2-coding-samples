import B_Tree
from collections import deque

my_tree = B_Tree.B_Tree(4)
for i in range(1, 19):
    my_tree.insert(i)
            
print("Main root keys:")
print(my_tree.root.keys)
print
print("Number of children:", len(my_tree.root.children))
print("Keys of children:")
for i, child in enumerate(my_tree.root.children):
    print("child", i)
    print(child.keys)
    
print("Testing recursive visualization:")
my_tree.visualize()
print
print("Now we'll try to delete 9")
my_tree.delete(9)
my_tree.visualize()
