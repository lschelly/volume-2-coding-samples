"""
Heap.py
Logan Schelly
3 November 2017
"""

#import the ceiling function for heapify.
from math import ceil

class Heap():
    """A class to implement the heap data structure"""
    
    def __init__(self, input_list = None):
        """Heapifies input list.  Resulting object is heaped version of that list."""
        #no input list results in empty one
        if(input_list == None):
            input_list = []
        #heapify the array passed in.
        self.array = self.heapify(input_list.copy())
   	
    def heapify(self, given_list):
        """heapifies any given list."""
        #use the procedure described before Proposition 3.4.7
        for i in range(ceil(len(given_list)/2)-1, -1, -1):
            self._sift_down(given_list, i)
        return given_list    
        
    def insert(self,value):
        """Inserts value into the heap."""
        #append the value to the array (leftmost empty leaf position).
        self.array.append(value)
        #get the index of the newly appended value
        index = len(self.array)-1
        #sift up the new value at the new index
        self._sift_up(self.array, index)
        return
    
    def remove(self, i):
        """Removes the value stored at index i"""
        #get the index of the rightmost leaf at the bottom of the tree.
        li = len(self.array)-1
        #swap the leaf value with the to be removed value.
        self.array[i], self.array[li] = self.array[li], self.array[i]
        #remove the leaf that now contains the value to be deleted.
        self.array.pop()
        #sift down the value that was moved up from the leaf.
        self._sift_down(self.array, i)
        return
    
    def _sift_up(self, a, k):
        """Sifts up node k in array a, until it is bigger than its parent"""
        #Have c stand for the current index of the node.
        c = k

        #If the node is not currently the root, and it is larger than its parent...
        while(c>0 and a[c] > a[self._parent(c)]):
            #Swap the value of the node with the value of its parent.
            a[c], a[self._parent(c)] = a[self._parent(c)], a[c]
            #Update the index of the node to the index of where its parent used to be.
            c = self._parent(c)

        #Once we exit the while loop, the node has been succesfully sifted up.
        return
        
    def _sift_down(self, a, k):
        """Sifts a node at index k in array a down until smaller than both children."""
        #Make a function for finding the index of the smallest child of a node.
        def smallest_child(x):
            rc = self._right_child(x)
            lc = self._left_child(x)
            if(rc >= len(a) or a[lc] <= a[rc]):
                return lc
            else:
                return rc

        #Have p stand for the current index of the node we are sifting down.
        p = k

        #If the node is not a leaf, and it is larger than its smallest child... 
        while(p<len(a)//2 and a[p] > a[smallest_child(p)]):
            sc = smallest_child(p)
            #Swap the value at the node with the value of its smallest child.
            a[p], a[sc] = a[sc], a[p]
            #Update the index of the node to the former index of the smallest child.
            p = sc

        #When we exit the loop, p is either a leaf or smaller than all of its children.
        return
    
    def _parent(self, n):
        return (n-1)//2     
        
    def _left_child(self, k):
        return 2*k+1
        
    def _right_child(self, k):
        return 2*k+2
        
    def __str__(self):
        return str(self.array)
        
    def __repr__(self):
        return "Heap("+self.__str__()+")"
                
                
                
                
                
