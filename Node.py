class Node:
    """A basic node class for storing data."""
    def __init__(self,data):
        """Store 'data' in the 'value' attribute"""
        self.value = data

class SinglyLinkedListNode(Node):
    """A node class for singly linked lists. Inherits from the 'Node' class.
    Contains a reference to the next node in the linked list.
    """
    def __init__(self, data):
        """Store 'data' in the 'value' attribute and initialize
        attributes for the next and previous nodes in the list.
        """
        Node.__init__(self, data)       # Use inheritance to set self.value.
        self.next = None

class DoublyLinkedListNode(Node):
    """A node class for doubly linked lists. Inherits from the 'Node' class.
    Contains references to the next and previous nodes in the linked list.
    """
    def __init__(self, data):
        """Store 'data' in the 'value' attribute and initialize
        attributes for the next and previous nodes in the list.
        """
        Node.__init__(self, data)       # Use inheritance to set self.value.
        self.prev = None
        self.next = None

class BSTNode(Node):
    """A node class for Binary Search Trees. Inherits from the 'Node' class.
    Contains some data, a reference to the parent node, and references to two child nodes.
    """
    def __init__(self,data)
        """Construct a new node and set the data attribute. The other
        attributes will be set when the node is added to a tree.
        """
        Node.__init__(self,data)        # Use inheritance to set self.value.
        self.prev = None
        self.left = None
        self.right = None

class BTreeNode(Node):
    """A node class for B-Trees. Inherits from the 'Node' class. Contains a list of data,
    a reference to the parent node, and a list of references to children.
    """
    def __init__(self,data)
        """Construct a new node and set the data attribute. The other attributes will be
        set when the node is added to a tree.
        """
        Node.__init__(self,data)        # Use inheritance to set self.value.
        self.prev = None
        self.children = None
