"""
B_Tree_With_TeX.py
Logan Schelly
6 January 2018

An implementation of a B_Tree, but with a method of writing to a LaTeX file, so it's easier to visualize what the computer is doing.
"""

#library for dealing with binary search/insert for sorted lists.
import bisect
from time import ctime

class B_Tree_With_Tex():   
        
    class B_tree_node():
        """
        A node for B-Trees.
        """
        def __init__(self, order, k=[], c=[], p=None):
            #have a list of keys
            self.keys = k
            #have a list of child nodes as well.
            self.children = c
            #keep track of the order in here as well
            self.order = order
            #keep track of parent as well
            self.parent = p
        
        def is_leaf(self):
            return len(self.children) == 0
        
        def overfull(self):
            return len(self.keys) > self.order
        
        def underfull(self):
            #underfull means not the root and less than half full.
            return self.parent is not None and len(self.keys) < self.order//2
        
        def has_extra_key(self):
            return len(self.keys) > self.order//2
    
    def __init__(self, order, record=False):
        """
        Initializes a B-Tree of a given order, where order is even.
        """
        self.order = order
        #uninitialized B-Tree has empty B_tree_node as a root.
        self.root = self.B_tree_node(self.order)
        
        #If we're recording everything to a Tex file, then get that set up.
        self.record = record
        if(self.record):
            #open up the tex_file we will write stuff to.
            record_file_name = "B Tree Record " + ctime() + ".tex"
            record_file_name = record_file_name.replace(' ', '_')
            self.tex_file = open(record_file_name, 'w')
            #Copy the setup of the template file.
            with open("B_Tree_Record_Template.tex") as template_file:
                for line in template_file.readlines():
                    if(line == '(Comment goes here.)\n'):
                        break
                    else: 
                        self.tex_file.write(line)
            return
                        
    def __del__(self):
        #Just properly close the tex_file if we're recording.
        if(self.record):
            self.tex_file.write("\\end{document}")
            self.tex_file.close()
                
    
    def find(self, target_key):
        """
        Returns true or false whether a given key is in the tree.
        If the key is in the tree, it returns the node where the key is at.
        If the key is not in the tree, it returns the leaf to insert the key at.
        """
        #Start at the root of the tree. 
        current_node = self.root
        
        #Check if the target key is in the current node.
        while(target_key not in current_node.keys):
            #If the target key is not in the current node...
            
            #Check if the current node is a leaf.
            if(current_node.is_leaf()):
                #If the current node is a leaf... it's not in the tree.
                return False, current_node
                
            else:#It's not a leaf.  Traverse towards children.
                #Use a binary search to find the index where the node keys
                #become larger than the target key.
                i = bisect.bisect(current_node.keys, target_key)
                #Use that index to traverse towards the correct child.
                current_node = current_node.children[i]
                            
        #If the target key is in the current node... we found it!
        return True, current_node
    
    def insert(self, insertion_key, record_it = False):
        """
        Inserts the given key into the B-Tree.
        """
        if(record_it and self.record):
            self.write_to_tex("Before inserting "+ str(insertion_key))
        #Find the appropriate insertion point.
        already_in_tree, insertion_leaf = self.find(insertion_key)

        #Is the key already in the tree?
        if(already_in_tree):
            return #no need to insert it again.

        #Otherwise, insert the key into the leaf's keys in order.
        bisect.insort(insertion_leaf.keys, insertion_key)

        #Label the insertion leaf as the current node.
        current_node = insertion_leaf
        
        if(record_it and self.record):
            self.write_to_tex("After inserting "+ str(insertion_key))

        #While the current node is overfull
        while(current_node is not None and current_node.overfull()):
            if(record_it and self.record):
                self.write_to_tex("About to split at"+ str(current_node.keys))
            #promote the median key, and make a new sibling to take some data.
            self._split(current_node)
            if(record_it and self.record):
                self.write_to_tex("After split")
            #Now check if the parent is overfull.
            current_node = current_node.parent
            
        #We're finished when the current node is no longer overfull.
        return    
   
    def delete(self, removal_key, record_it = False):
        """
        Deletes the key from the B-Tree
        """
        #Find the node that the key is at.
        in_tree, node = self.find(removal_key)
        
        #If it's not in the tree to begin with, then we're done.
        if(not in_tree):
            return
        
        #Make sure the key is in a leaf.
        if(node.is_leaf()):
            leaf = node
            
        else: #If it's not in a leaf, put it in a leaf like this:
            i = node.keys.index(removal_key)
            #Find the leaf its in-order sucessor is in.
            leaf = self._get_successor_leaf(node, i)
            #Swap it with its successor, so it is in a leaf.
            leaf.keys[0], node.keys[i] = node.keys[0], leaf.keys[i]
            
        #Remove the key from the leaf.
        leaf.keys.remove(removal_key)
            
        #Start dealing with underfill at the leaf of removal.
        current = leaf
        
        #Check to see if the current node is underfull.
        while(current.underfull()):
            #To fix underfill, first try to take a key from a sibling.
            try:
                self._take_from_sibling(current)
                #Taking a key from a sibling, rebalances the tree.
                return
                
            except ValueError: 
                #If you cannot take from a sibling, merge with one.
                self._merge_with_sibling(current)
                
                #If the merged node's parent is the root...
                if(current.parent is self.root):
                    #Just make sure to deal with an empty root...
                    if(len(self.root.keys)==0):
                        #by making the new merged node the root.
                        self.root = current
                    #Then you're done dealing with underfill!
                    return
                    
                else: #For non-root parents, check the parent node for underfill.
                    current = current.parent

        #If the node is not underfull, you're done.
        return
                    
                    
    def _split(self, node):
        """Promotes the median key into the parent,
        and creates a new sibling to take the larger keys/children."""
        
        #Find the median key in the node.
        middle = len(node.keys)//2
        median = node.keys[middle]
        
        #Make the new sibling
        sibling = self.B_tree_node(self.order)
        sibling.keys = node.keys[middle+1:]
        middle_child = len(node.children)//2
        sibling.children = node.children[middle_child:]
        for child in sibling.children:
            child.parent = sibling
        
        #Update the node, to reflect that it gives away keys/children
        node.keys = node.keys[:middle]
        node.children = node.children[:middle_child]
        
        if(node.parent is not None):
            #Find the correct insertion point for the median into the parent.
            i = bisect.bisect(node.parent.keys, median)
            #Insert the median into the parent keys.
            node.parent.keys.insert(i, median)
            #Node was already at index i, so insert sibling at i+1.
            node.parent.children.insert(i+1, sibling)
            sibling.parent = node.parent
            return
        
        elif(node is self.root):
            #Make a new root.
            new_root = self.B_tree_node(self.order, k=[median], c=[node, sibling])
            node.parent = new_root
            sibling.parent = new_root
            self.root = new_root
            return
        
        else:
            raise ValueError("split got a bogus argument.")   
    
    def _get_successor_leaf(self, local_root, index_of_key):
        """
        Returns the leaf where the in-order successor is stored. 
        """
        current = local_root.children[index_of_key+1]
        while(not current.is_leaf()):
            current = current.children[0]
        return current    
        
    def _take_from_sibling(self, node):
        """ Takes a key from the parent, and replaces it with a sibling key """    
        
        #Find what index in the parent this node belongs in.
        i = bisect.bisect(node.parent.keys, node.keys[0])
        
        #Check the left sibling for an extra key.
        left_sib = node.parent.children[i-1] if i>0 else None
        if(left_sib != None and left_sib.has_extra_key()):
            #Put the parent key into the node.
            node.keys.insert(0, node.parent.keys[i-1])
            #Remove the largest sibling key, and write it over the parent key.
            node.parent.keys[i-1] = left_sib.keys.pop()
            #Adopt the sibling's last child as the new first child.
            if(left_sib.children):
                node.children.insert(0,left_sib.children.pop())
                node.children[0].parent = node
            return
            
        #If the left sibling doesn't have a key, check the right sibling.
        if(i < len(node.parent.children)-1):
            right_sib = node.parent.children[i+1]
        else:
            right_sib = None
            
        if(right_sib != None and right_sib.has_extra_key()):
            #Put the parent key into the node
            node.keys.append(node.parent.keys[i])
            #Remove the smallest sibling key, and write it over the parent key.
            node.parent.keys[i] = right_sib.keys.pop(0)
            #Adopt the sibling's first child as the new last child.
            if(right_sib.children):
                node.children.append(right_sib.children.pop(0))
                node.children[-1].parent = node
            return
            
        #If you can't take from either sibling, raise a value error
        else:
            raise ValueError 
            
    
    def _merge_with_sibling(self, node):
        """Absorbs the sibling into the node"""
        
        #Find what index in the parent this node belongs in.
        i = bisect.bisect(node.parent.keys, node.keys[0])
        
        #Try the left sibling if it's available.
        if(i>0):
            #Remove it from the parent's list of children, and hold on to it.
            sib = node.parent.children.pop(i-1)
            #Prepend the parent key, then the sibling keys
            node.keys.insert(0,node.parent.keys.pop(i-1))
            node.keys = sib.keys + node.keys
            #Adopt the sibling children.
            node.children = sib.children + node.children
        
        else:#Otherwise, use the right sibling.
            sib = node.parent.children.pop(i+1)
            node.keys.append(node.parent.keys.pop(i))
            node.keys.extend(sib.keys)
            node.children.extend(sib.children)
        
        #Make sure the sibling's children adopt the new, merged node.
        for child in sib.children:
            child.parent = node
        
        #The sibling should get garbage collected.
        del sib
        return
        
            
            
    def visualize(self):
        """
        Prints the tree sideways with the root at the left.
        Not the best visualization, but easy to code.
        """
        def print_recursive(node, depth):
            if(node.is_leaf()):
                print(' '*(depth*4) , node.keys)
            else:
                for i, c in enumerate(node.children[::-1]):
                    print_recursive(c, depth+1)
                    if(i < len(node.keys)):
                        print(' '*(depth*4), node.keys[::-1][i])
                                
        print_recursive(self.root, 0)
        return
        
    def write_to_tex(self, comment, additional_comment = ""):
        if(not self.record):
            return
        else:
            #use this recursive function to write the tex file.
            def recursive_tree_write(node, depth=1):
                self.tex_file.write("node{"+ str(node.keys)[1:-1] + "}\n")
                for c in node.children:
                    self.tex_file.write(" "*(4*(depth)) + "child {")
                    recursive_tree_write(c, depth = depth+1)
                    self.tex_file.write(" "*(4*depth) + "}\n")
            #Start the write at the root
            self.tex_file.write("\\verb!"+ comment+ "!\n")
            self.tex_file.write("\\begin{center}\n")
            self.tex_file.write("\\begin{tikzpicture}[B-Tree]\n")
            self.tex_file.write("\\")
            if(self.root is not None):
                recursive_tree_write(self.root)
                self.tex_file.write(";")
            self.tex_file.write("\end{tikzpicture}\n")
            self.tex_file.write("\end{center}\n")
            self.tex_file.write("\\verb!" + additional_comment + "!\n")
            return
