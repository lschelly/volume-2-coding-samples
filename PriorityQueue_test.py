import string
from PriorityQueue import priority_queue

alphabet = string.ascii_lowercase

x = [(alphabet[i], i) for i in range(26)]

print("This is x:")
print(x)

print()
print("Putting everything in x into a priority queue.")
q = priority_queue(x)

print()
print("Now trying it with the syntax from the book.")
q2 = priority_queue((alphabet[i], i) for i in range(26))

print()
print("This is the heap for q2:")
print(q2.heap)

print()
print("Updating the priority of z to -1.")
q2.update_priority(('z', -1))
print("This is now the heap for q2:")
print(q2.heap)

print()
print("Using network X to test Algorithm 4.2.1")

from math import inf

#####################################################################
#Dr. Jarvis's implementation of Dijkstra's Algrithm
#####################################################################
def dijkstra(G, s, t):
    """ Find the shortest path from s to to in G """
    
    """ Initialize """
    #best known path lengths.
    d = {u:inf for u in G.nodes()}
    d[s] = 0
    #Immediate predecessors for best-known path.
    pred = dict()
    #Priority Queue of nodes not yet processed.
    Q = priority_queue((u, d[u]) for u in G.nodes())
    #make w
    w = {e:G.get_edge_data(*e)['weight']  for e in G.edges()}
    w.update({e[::-1]: G.get_edge_data(*e)['weight'] for e in G.edges()})

    """ Find optimal paths. """
    while len(Q)>0:
        v = Q.pop_min()
        if v == t:
            break
        else:
            for u in G.neighbors(v):
                if d[v] + w[v,u] < d[u]:
                    #update the best-known distance to u.
                    d[u] = d[v] + G.get_edge_data(v, u)['weight']
                    #update the best-known predecessor of u.
                    pred[u] = v
                    #update Q
                    Q.update_priority((u, d[u]))
                    
    """ Reconstruct the optimal path from t to s. """  
    optimal_path = [t]
    while optimal_path[-1] != s:
        optimal_path.append(pred[optimal_path[-1]])
        
    return list(reversed(optimal_path)), d[t]
    
##############
#Testing it
#                      
import networkx as nx

print()
print("Example 4.2.3")
G = nx.Graph()
G.add_weighted_edges_from([('A', 'B', 2),
                            ('A', 'D', 5),
                            ('B', 'C', 5), 
                            ('B', 'D', 2), 
                            ('B', 'E', 7),
                            ('B', 'F', 9),
                            ('C', 'F', 2),
                            ('D', 'E', 4),
                            ('E', 'F', 3)])
path, distance = dijkstra(G, 'A', 'F')
print("Best path:", path)
print("distance:", distance)
print()
print("Problem 4.7")
G.clear()
G.add_weighted_edges_from([('A', 'B', 3),
                            ('A', 'F', 6),
                            ('B', 'C', 5),
                            ('B', 'F', 4),
                            ('C', 'D', 1),
                            ('C', 'E', 1),
                            ('C', 'F', 5),
                            ('D', 'E', 2),
                            ('E', 'F', 4)])
for destination in ['F', 'E', 'D']:
    print("    A to", destination)
    path, distance = dijkstra(G, 'A', destination)
    print(path)
    print(distance)
