"""
LinkedList.py
A file with the specifications for a linked list class
"""
from Node.py import SinglyLinkedListNode

class SinglyLinkedList():
    """
    Singly linked list data structure class
    
    Attributes:
        head (SinglyLinkedListNode): the first node in the list
    """
    def __init__(self):
        """
        Initialize the head to be None, because the list is initially empty.
        """
        #store the head of the list.
        self.Head = None
    
    def add(self, value, index=0):
        """
        Add a node at a given index in a list.
        """
        #start at the head.
        current = self.Head
        #iterate through the nodes until just before that index
        for i in range(index-1):
            if(current = None):
                throw 
