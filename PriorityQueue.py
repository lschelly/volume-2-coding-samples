from Heap import Heap

class priority_queue:
    """A class that implements the priority queue data structure."""
    
    class priority_queue_node:
        """A class to handle the nodes/elements of a priority queue."""
        def __init__(self, input_tuple):
            """First value of input_tuple is data.  Second is priority."""
            self.data = input_tuple[0]
            self.priority = input_tuple[1]
        
        """Define comparison operators between nodes.  Compare by priority."""    
        def __lt__(self, other):
            return self.priority < other.priority
            
        def __le__(self, other):
            return self.priority <= other.priority
        
        def __gt__(self, other):
            return self.priority > other.priority
            
        def __ge__(self, other):
            return self.priority >= other.priority
        
        def __eq__(self, other):
            return self.priority == other.priority
        
        def __neq__(self, other):
            return self.priority != other.priority
            
        """Also give string and repr methods"""
        def __str__(self):
            return str((self.data, self.priority))
        
        def __repr__(self):
            return "pqnode"+(self.data, self.priority).__repr__()
    
    def __init__(self, nodes):
        """Constructor.  
        Nodes is an iterable with tuples.
        The first entry of each tuple is the data.
        The second entry is the priority."""   
        self.heap = Heap([self.priority_queue_node((a[0], a[1])) for a in nodes])
    
    def pop_min(self):
        #get the data in the minimal node.
        d = self.heap.array[0].data
        #remove the minimal node.
        self.heap.remove(0)
        #return the data.
        return d
        
    def find_min(self):
        #get the data in the minimal node, and return it.
        return self.heap.array[0].data
        
    def update_priority(self, input_tuple):
        """
        input tuple will have the data in the 1st entry, and the new priority in the 2nd.
        """
        search_data = input_tuple[0]
        #search through the array, until we find the node with the data
        for i, node in enumerate(self.heap.array):
            if(node.data == search_data):
                #remove this node from the heap.
                self.heap.remove(i)
                #insert a new node with the same value, but different priority.
                self.heap.insert(self.priority_queue_node(input_tuple))
                #Note: there exist more efficient ways to update priority.
                return
        #if we go through the whole queue and don't find the data, throw an error
        raise ValueError("No node has the key ", input_tuple[0])
        
    def __len__(self):
        return len(self.heap.array)
